# Introductie (15m)

# Getting Started - DigitalOcean (1h)

De Terraform website biedt uitstekende tutorials aan om kennis te maken met Terraform. We gaan gebruik maken van een bestaande getting started tutorial voor AWS, https://learn.hashicorp.com/collections/terraform/aws-get-started. We willen echter onze infrastructuur op DigitalOcean beheren, dus we gaan de AWS-specifieke elementen vertalen naar de equivalente DigitalOcean elementen. De documentatie voor de DigitalOcean provider voor terraform is te vinden op https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs.

De documentatie beschrijft hoe je de provider configureert, welke resources worden aangeboden en hoe je daar gebruik van maakt. Om de provider op te zetten, zijn we een DigitalOcean-token nodig. Hiermee autoriseert DigitalOcean de api-calls die Terraform in de achtergrond uitvoert om onze infrastructuur op te zetten. Voor deze sessie maken we gebruik van een token dat gekoppeld is aan mijn DigitalOcean account.

* Volg de tutorials (behalve de laatste) in de genoemde link met de volgende verschillen:
    * Build Infrastructure
        * De aws-cli tool hoeft niet geinstalleerd te worden
        * Definieer de DigitalOcean provider ipv de AWS provider met het volgende block:
        
            ```
            terraform {
              required_providers {
                digitalocean = {
                  source  = "digitalocean/digitalocean"
                  version = "~> 2.3.0"
                }
              }
            }
            ```
        * Configureer de DigitalOcean provider (check de provider docs) met de gegeven token.
        * Maak een `digitalocean_droplet` resource aan ipv een `aws_instance` (check de provider resource docs). Gebruik een unieke waarde voor `name`, zodat we elkaar niet in de weg zitten.
    * Change Infrastructure
        * Vervang de waarde van `image` met een van de Distro Images slugs voor DigitalOcean, te vinden op https://slugs.do-api.dev.
    * Define Input Variables
        * Gebruik `region` waardes voor DigitalOcean, te vinden op https://slugs.do-api.dev
        * Maak een output block die de `image` waarde van de droplet bevat ipv de aws instance ami.
        * Als je de tutorial hebt voltooid, probeer eens alle waarden van de droplet te extraheren in variabelen.
    * Query Data with Output Vaiables
        * Maak een `digitalocean_floating_ip` resource aan ipv een aws_eip (check de provider docs) en output de waarde `ip_address` van deze resource.
    * Als je klaar bent:
        * Destroy alle opgezette infrastructuur.

# Een stukje infrastructuur opzetten in DigitalOcean (1h)
Het doel van deze sectie is om terraform te gebruiken om iteratief een infrastructuur op te zetten. We beginnen met een droplet en gaan daar een applicatie op deployen. Vervolgens gaan we resources toevoegen die wat meer functionaliteit toevoegen aan de infrastructuur.

De instructies zijn redelijk open-ended, in de zin dat ik niet specificeer wat de precieze configuratie van alle resources moet zijn. De terraform-documentatie geeft voor elke resource een minimale redelijke configuratie, dus dat is een goed startpunt. Gebruik de kennis die je hebt opgedaan met de tutorials en voel je vrij om te experimenteren met de optionele configuratie. 
* Probeer enigszins redelijk te zijn met wat je configureert, dus niet 32-core droplets opzetten bijvoorbeeld.
* Hou er ook rekening mee dat we met zijn allen op hetzelfde account resources opzetten, dus zorg ervoor dat je resources unieke namen hebben (gebruik bijvoorbeeld je eigen naam als prefix)

## Opzetten van een droplet en inloggen via SSH
We beginnen met het opzetten van een droplet via terraform die bereikbaar over SSH met een ssh-keypair.

1. Maak een ssh keypair aan (zie stap 1 in https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/ voor instructies) en noteer de path van de public key. De content van de private key hebben we later ook nodig.

2. Maak een digitalocean_ssh_key resource aan met de public key path en gebruik deze resource bij het maken van een digitalocean_droplet resource (zie https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/ssh_key). Gebruik `docker-20-04` als image.

3. Verifieer dat je in kunt loggen als gebruiker `root` op het ip-adres van de droplet (zie https://www.digitalocean.com/docs/droplets/how-to/connect-with-ssh/ voor hulp).

## Deployen van de Micronaut applicatie
We deployen een dockerized Micronaut applicatie op de droplet met de CI pipeline die in `.gitlab-ci.yml` beschreven is.

1. Fork dit Gitlab project met je eigen Gitlab account. (https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
2. In het project, onder `Settings > CI/CD > Variables`, voeg twee variabelen toe:
    * Key:Value = SERVER_IP:<ip adres van droplet\>, Type = Variable, Protect variable = unchecked, Mask variable = unchecked
    * Key:Value = SERVER_SSH_KEY:<content van private key, inclusief de eerste en laatste regel die BEGIN en END bevatten\>, Type = File, Protect variable = unchecked, Mask variable = unchecked
3. Draai de CI/CD Pipeline en verifieer dat de applicatie draait op de droplet door naar `http://<ip-adres>:8080/hello` te browsen.

Hou er rekening mee dat je droplet resource na een destroy-apply cyclus een ander ip heeft. Op dat moment moet de eerste variable aangepast worden, zodat Gitlab naar de nieuwe droplet deployt.

## Load balancing
We maken een tweede droplet en zetten daar een load balancer voor.

1. Zet een `digitalocean_loadbalancer` resource op met een `forwarding_rule` block die http verkeer op poort 80 naar poort 8080 mapt. 
2. Voeg een 'healthcheck' block toe. De healthcheck-endpoint van de applicatie is `/health` op poort 8080
3. Maak een tweede droplet resource. Maak een nieuwe variabele aan voor het ip van de nieuwe droplet en pas in `.gitlab-ci.yml` de `deploy_staging` job aan, zodat de docker image op beide droplets gedeployt wordt. 
4. Voeg de nieuwe droplet toe aan de `digitalocean_loadbalancer` resource
5. Draai de CI pipeline opnieuw en verifieer dat alles nog werkt.

## Configureerbare infrastructuur

We hebben een klein stukje infrastructuur gemaakt dat we op een later moment misschien willen hergebruiken. Etraheer naar eigen inzicht variabelen uit de resource blocks (doplet size, aantal droplets etc)